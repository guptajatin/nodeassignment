function forEach(array, callback) {
    for(var i=0;i<array.length;i++){
        callback(array[i]);
    }
}
function map(array, callback) {
    var x=[];
    for(var i=0;i<array.length;i++){
         x[i]= callback(array[i]);
    }
    return x;
}
var x=[1,2,3];
function funcForEach(y){
    console.log(y);
}
forEach(x,funcForEach);
function funcMap(i) {
    return i*2;
}
var ans = map(x,funcMap);
console.log(ans);

function wait(ms){
    var start = new Date().getTime();
    var end = start;
    while(end < start + ms) {
        end = new Date().getTime();
    }
}
function setTimeoutSync(callback, n) {
    wait(n);
    callback();
}
function helloworld(){
    console.log('hello world');
}
setTimeoutSync(helloworld, 5000);