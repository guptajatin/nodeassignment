var express = require('express');

var app = express();

var bodyParser = require('body-parser');

app.set("view engine", "pug");

var morgan = require('morgan');

var fs = require('fs');

var path = require('path');

var writeFile = fs.createWriteStream(path.join(__dirname,'test.log'), {flags:'a'});

app.use(morgan('combined', {stream:writeFile}));

app.use("/", bodyParser.urlencoded({extended:false}) );

app.use("/assets", express.static(__dirname+"/assets"));

app.use('/form', function (req, res) {
    res.sendFile(__dirname+'/assets/form.html');
});

app.post("/index", function (req,res) {
    var name = req.body.name;
    var bday = req.body.bday;
    var newBirthdayFormatted =  new Date(bday);
    var today = new Date();
    res.render("index.pug",{name:name, x: Math.round((today-newBirthdayFormatted)/(1000*60*60*24))});
});

app.use('/', function (req, res) {
    res.send('Hello Viewer!');
});

app.listen(3000);